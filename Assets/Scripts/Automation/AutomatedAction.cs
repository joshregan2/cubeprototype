using UnityEngine;
using System.Collections;

public class AutomatedAction
{
	private float Counter;
	public float Duration {get; set;}
	public EventDelegate ActionDelegate {get; set;}
	public bool IsComplete {get; private set;}

	public AutomatedAction(float duration, EventDelegate actionDelegate)
	{
		Counter = 0;
		Duration = duration;
		ActionDelegate = actionDelegate;
		IsComplete = false;
	}

	public void Update()
	{

		if (IsComplete == false && Counter < Duration)
		{
			Counter += Time.deltaTime;
			if (ActionDelegate != null)
			{
				ActionDelegate.parameters.SetValue(Counter, 0);
				ActionDelegate.parameters.SetValue(Duration, 1);
				ActionDelegate.Execute();
			}
		}
		else
		{
			IsComplete = true;

		}
	}
}

