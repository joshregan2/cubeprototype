﻿using UnityEngine;
using System.Collections;

public class Camera3D : MonoBehaviour 
{
	#region Fields and Properties

	private float previousPointerX;
	private float previousPointerY;

	private Vector3 originalPosition;
	private Quaternion originalRotation;
	private Vector3 originalLocalScale;

	public bool IsAttachedToPlayer {get; private set;}
	public bool IsFollowEnabled {get; private set;}

	// The target we are following
	private Transform target;
	// The distance in the x-z plane to the target
	private float distance = 0.25f;
	// the height we want the camera to be above the target
	private float height = 5.5f;
	// How much we 
	private float heightDamping = 2.0f;
	private float rotationDamping = 3.0f;

	#endregion

	#region Lifecycle

	void Awake()
	{
		previousPointerX = 0;
		previousPointerY = 0;
		IsAttachedToPlayer = false;
		IsFollowEnabled = false;
	}

	void Start()
	{
		originalPosition = gameObject.transform.position;
		originalRotation = gameObject.transform.rotation;
		originalLocalScale = gameObject.transform.localScale;
	}

	void Update() 
	{
		HandleUserInput();
	}
	
	void LateUpdate() 
	{
		// Early out if we don't have a target
		if (target != null && IsFollowEnabled == true)
		{
			// Calculate the current rotation angles
			float wantedRotationAngleX = target.eulerAngles.x;
			float wantedRotationAngleY = target.eulerAngles.y;
			float wantedRotationAngleZ = target.eulerAngles.z;
			float wantedHeight = target.position.y + height;

			float currentRotationAngleX = transform.eulerAngles.x;
			float currentRotationAngleY = transform.eulerAngles.y;
			float currentRotationAngleZ = transform.eulerAngles.z;
			float currentHeight = transform.position.y;
			
			// Damp the rotation around the y-axis
			currentRotationAngleX = Mathf.LerpAngle(currentRotationAngleX, wantedRotationAngleX, rotationDamping * Time.deltaTime);
			currentRotationAngleY = Mathf.LerpAngle(currentRotationAngleY, wantedRotationAngleY, rotationDamping * Time.deltaTime);
			currentRotationAngleZ = Mathf.LerpAngle(currentRotationAngleZ, wantedRotationAngleZ, rotationDamping * Time.deltaTime);
			
			// Damp the height
			currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);
			
			// Convert the angle into a rotation
			Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngleY, 0);
			
			// Set the position of the camera on the x-z plane to:
			// distance meters behind the target
//			transform.position = target.position;
//			transform.position -= target.forward * distance;

			Vector3 newPosition = target.position - (Vector3.forward * distance);
			newPosition = (new Vector3(newPosition.x, newPosition.y, newPosition.z)) + (target.up * height);

//			newPosition = Vector3.Lerp(transform.position, newPosition, rotationDamping * Time.deltaTime);

			// Set the height of the camera
			transform.position = newPosition;

			// Always look at the target
			transform.LookAt(target);
		}
	}

	#endregion

	#region Camera Movement

	public void AttachCameraToPlayer()
	{
		IsAttachedToPlayer = true;
		target = GameManager.SharedInstance.EntityManager.ActivePlayer.transform;
		IsFollowEnabled = true;
		gameObject.transform.parent = target.parent;
	}

	public void AttachCameraToWorld()
	{
		gameObject.transform.parent = null;
		IsAttachedToPlayer = false;
		IsFollowEnabled = false;
		target = null;

		float tweenDuration = 1.0f;

		TweenRotation.Begin(gameObject, tweenDuration, originalRotation);
		TweenPosition.Begin(gameObject, tweenDuration, originalPosition);
	}


	#endregion

	#region User Input

	private void HandleUserInput()
	{
		if (Input.GetKeyUp(KeyCode.C))
		{
			if (IsAttachedToPlayer == false)
			{
				AttachCameraToPlayer();
			}
			else 
			{
				AttachCameraToWorld();
			}
		}
	}


	#endregion
}
