using UnityEngine;
using System.Collections;

public class Gateway : MonoBehaviour
{
	public bool IsPlayerAbleToInteract {get; private set;}
	public string GatewayId {get; set;}

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		if (IsPlayerAbleToInteract == true)
		{
			if (Input.GetKeyUp(KeyCode.E))
			{
			}

		}
	}

	void OnTriggerEnter(Collider collider)
	{
		Debug.Log("Interact With Me!");
		IsPlayerAbleToInteract = true;
	}
}

