using UnityEngine;
using System.Collections;

public class Interactive : MonoBehaviour
{
	public bool IsPlayerAbleToInteract {get; private set;}
	public EventDelegate InteractiveAction {get; set;}

	// Use this for initialization
	void Start ()
	{
		IsPlayerAbleToInteract = false;
	}

	// Update is called once per frame
	void Update ()
	{
		if (IsPlayerAbleToInteract == true)
		{
			if (Input.GetKeyUp(KeyCode.E))
			{
				if (InteractiveAction != null)
				{
					InteractiveAction.Execute();
				}
			}
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		Debug.Log("Interact With Me!");
		IsPlayerAbleToInteract = true;
	}
}

