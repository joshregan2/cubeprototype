using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	#region Fields and Properties

	private const float PLAYER_MOVE_SPEED = 8.0f;
	private const float PLAYER_GRAVITY = 10.0f;

	public bool IsPlayerPaused {get; set; }

	#endregion

	#region Lifecycle

	void Awake()
	{
		IsPlayerPaused = false;
	}

	void Start()
	{
		Rigidbody body = gameObject.AddComponent<Rigidbody>();
		body.useGravity = false;
		body.freezeRotation = true;

		SphereCollider sphereCollider = gameObject.AddComponent<SphereCollider>();
		sphereCollider.radius = 0.06f;
	}

	void Update()
	{
		if (IsPlayerPaused == false) {
			AlignToFloor ();

			PlayerMovement ();
		}
	}

	#endregion

	#region Artificial Gravity

	private void AlignToFloor()
	{
		Vector3 directionToCenter = gameObject.transform.parent.position - gameObject.transform.position;
		directionToCenter.Normalize();

		Vector3 upVector = Vector3.up;

		RaycastHit hit;
		if (Physics.Raycast(gameObject.transform.position, directionToCenter, out hit))
		{
			upVector = hit.normal;
		}

		gameObject.transform.up = upVector;
		gameObject.rigidbody.AddForce(-upVector * PLAYER_GRAVITY);

	}

	#endregion

	#region Movement

	public void PlayerMovement()
	{
		Vector3 movement = Vector3.zero;
		if (Input.GetKey(KeyCode.W) == true)
		{
			movement += new Vector3(0, 0, PLAYER_MOVE_SPEED);
		}
		else if (Input.GetKey(KeyCode.S) == true)
		{
			movement += new Vector3(0, 0, -PLAYER_MOVE_SPEED);
		}
		if (Input.GetKey(KeyCode.A) == true)
		{
			movement += new Vector3(-PLAYER_MOVE_SPEED, 0, 0);
		}
		else if (Input.GetKey(KeyCode.D) == true)
		{
			movement += new Vector3(PLAYER_MOVE_SPEED, 0, 0);
		}

		movement = Camera.main.transform.TransformDirection(movement);

		if (movement != Vector3.zero)
		{
			gameObject.rigidbody.AddRelativeForce(movement);

			float angle = Mathf.Atan2(movement.z, -movement.x);
			gameObject.transform.Rotate(new Vector3(0,1,0), angle * Mathf.Rad2Deg);
		}
	}

	public void TeleportToLocation(Vector3 destination)
	{
		TweenScale.Begin(gameObject, 0.1f, new Vector3(1.2f,1.2f,1.2f)).AddOnFinished(new EventDelegate(TeleportBeginScaleDown));
	}

	private void TeleportBeginScaleUp()
	{

	}

	private void TeleportBeginScaleDown()
	{

	}

	#endregion
}

