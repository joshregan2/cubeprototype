using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeMapConfig
{
	public const string MATERIALS_BASE_PATH = "Materials/";


	public static readonly Dictionary<string, CubeSurfaceInfo> CubeMapSurfaceTypes = new Dictionary<string, CubeSurfaceInfo>()
	{
		{ "0", new CubeSurfaceInfo(false) },
		{ "1", new CubeSurfaceInfo("", "Models/Environment/Forest/forest-tree-prefab") },
		{ "2", new CubeSurfaceInfo("", "Models/Environment/Forest/forest-tall-tree-prefab") },
		{ "3", new CubeSurfaceInfo("", "Models/Environment/Forest/light-post-prefab") },
		{ "G", new CubeSurfaceInfo("", "Models/Environment/Forest/gateway-prefab") },
		{ "8", new CubeSurfaceInfo(true) }
	};


	public CubeMapConfig()
	{

	}
}

