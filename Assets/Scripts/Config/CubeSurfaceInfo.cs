using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeSurfaceInfo
{
	public string PrefabFile { get; set; }
	public string MaterialFile { get; set; }
	public bool IsPlayerStart { get; set; }
	public bool IsBlock {get; set;}

	public CubeSurfaceInfo()
	{
		IsPlayerStart = false;
	}

	public CubeSurfaceInfo(bool isPlayerStart)
	{
		IsPlayerStart = isPlayerStart;
		IsBlock = false;
	}

	public CubeSurfaceInfo(string materialFile, string prefabFile) : this()
	{
		MaterialFile = materialFile;
		PrefabFile = prefabFile;
		IsBlock = !HasPrefabFile();
	}

	public bool HasPrefabFile()
	{
		return PrefabFile == null || PrefabFile.Length == 0 ? false : true;
	}
}

