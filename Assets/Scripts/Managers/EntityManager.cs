using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EntityManager
{
	#region Fields and Properties

	public List<GameObject> AllEntities {get; set;}

	public Player ActivePlayer {get;set;}

	#endregion

	#region Lifecycle

	public EntityManager()
	{
		AllEntities = new List<GameObject>();
		ActivePlayer = null;
	}

	public void Update()
	{

	}

	#endregion

	#region Player Creation

	public void CreatePlayer(Vector3 position, Transform parentTransform)
	{
		GameObject player = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Models/Characters/geo-man-prefab"));
		player.name = "Player";
		player.transform.parent = parentTransform;
		player.transform.position = parentTransform.position + position;
//		player.transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);

		ActivePlayer = player.AddComponent<Player>();
	}

	#endregion
}

