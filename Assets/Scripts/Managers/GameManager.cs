﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{

	#region Fields and Properties

	public static GameManager SharedInstance;

	public WorldManager WorldManager {get; set;}

	public EntityManager EntityManager {get; set;}

	public WorldCube CurrentWorldCube {get; set;}

	public Camera3D Camera3D {get; set;}

	#endregion

	#region Lifecycle

	void Awake()
	{
		// Store static singleton instance
		if (SharedInstance == null)
		{
			SharedInstance = this;
		}

		// Setup manager objects
		WorldManager = new WorldManager();
		EntityManager = new EntityManager();

		// Configure input for touch
		Input.simulateMouseWithTouches = true;
	}
	
	void Start() 
	{
		Camera3D = GameObject.Find("Main Camera").GetComponent<Camera3D>();

		InitialiseGame();
	}

	void Update() 
	{
	
	}

	#endregion

	#region Startup

	private void InitialiseGame()
	{
		CurrentWorldCube = WorldManager.CreateWorldCube(5, typeof(FirstArea)); // GameObject.Find("World Cube").GetComponent<WorldCube>();
	}

	#endregion
}
