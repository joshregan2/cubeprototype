using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WorldManager
{
	#region Fields and Properties

	public List<WorldCube> AllWorldCubes {get; set;}

	#endregion

	#region Lifecycle

	public WorldManager()
	{
		AllWorldCubes = new List<WorldCube>();
	}

	public void Update()
	{

	}

	#endregion

	#region World Cube Creation

	public WorldCube CreateWorldCube(float size, Type worldCubeBehaviour)
	{
		WorldCube newWorldCube = (new GameObject("World Cube")).AddComponent<WorldCube>();
		newWorldCube.transform.position = new Vector3(0, 0.8f, 0);
		newWorldCube.gameObject.AddComponent(worldCubeBehaviour);

		return newWorldCube;
	}

	#endregion
}

