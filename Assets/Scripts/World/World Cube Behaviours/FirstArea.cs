using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FirstArea : MonoBehaviour
{
	public WorldCube WorldCube {get; set;}

	void Awake()
	{

	}

	void Start()
	{
		WorldCube = this.gameObject.GetComponent<WorldCube>();
//		WorldCube.BuildOuterCube(5, Resources.Load<Material>("Materials/Glass Material"));

		string[,] faceMapTop = new string[,]
		{
			{"1","1","1","2","1","1","2","1","1","2","0","1","2","1","1","1","1","1","1","1"}, 
			{"1","0","0","0","3","1","1","2","1","1","0","1","1","2","2","2","2","2","2","1"}, 
			{"1","0","0","0","0","0","0","0","0","0","0","1","1","1","2","3","G","3","2","1"}, 
			{"2","1","1","2","1","1","2","1","1","1","0","1","2","1","2","0","0","0","2","1"}, 
			{"1","1","1","1","1","1","1","1","2","2","0","1","1","1","2","3","G","3","2","1"}, 
			{"0","0","0","0","0","1","2","1","2","1","0","3","1","1","2","2","2","2","2","1"}, 
			{"1","1","1","3","0","1","1","1","2","1","0","0","0","0","1","1","1","1","1","1"}, 
			{"1","1","1","1","0","1","1","1","1","1","1","1","3","0","1","1","2","1","2","1"}, 
			{"1","1","2","2","0","2","2","1","1","1","1","1","1","0","1","2","1","1","1","1"}, 
			{"1","1","2","3","0","3","2","1","2","1","1","1","1","0","2","1","1","2","1","1"}, 
			{"1","1","2","0","0","0","0","0","0","0","0","0","0","0","1","1","1","1","1","2"}, 
			{"1","1","2","0","0","0","2","2","2","2","1","1","1","1","1","2","1","1","1","1"}, 
			{"1","0","0","0","3","0","2","1","1","1","1","1","1","1","1","1","1","2","1","1"}, 
			{"3","0","2","2","2","0","2","2","2","1","2","1","1","2","1","1","0","0","0","1"}, 
			{"1","0","1","1","2","0","2","1","1","1","1","1","1","2","1","1","0","3","0","1"}, 
			{"1","0","1","1","1","0","0","0","0","0","0","0","0","0","0","0","0","1","0","1"}, 
			{"1","0","1","1","1","1","1","0","2","2","2","0","1","1","1","2","1","1","0","1"}, 
			{"G","0","2","1","2","1","1","0","2","3","2","0","1","1","2","1","1","1","0","1"}, 
			{"3","0","2","1","1","1","1","0","0","0","0","0","2","2","1","1","1","2","0","2"}, 
			{"G","2","2","1","2","1","1","1","2","1","1","2","1","1","1","2","1","3","G","3"}
		};

		string[,] faceMapBottom = new string[,]
		{
			{"1","2","2","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"}, 
			{"1","2","0","0","0","0","1","0","0","0","0","0","0","0","0","1","1","1","1","1"}, 
			{"1","2","2","2","2","0","1","0","2","2","2","2","2","2","0","1","1","1","1","1"}, 
			{"1","0","0","2","2","0","1","0","0","0","0","0","0","2","0","2","2","2","2","3"}, 
			{"1","0","0","2","2","0","1","0","2","2","0","0","0","2","0","2","0","0","0","G"}, 
			{"1","0","0","0","0","0","1","0","1","2","2","2","2","2","0","2","0","2","2","3"}, 
			{"1","1","1","1","1","0","1","0","1","1","0","0","0","0","0","0","0","1","1","1"}, 
			{"2","2","2","3","1","0","1","0","1","1","0","1","1","0","0","0","0","1","1","1"}, 
			{"2","0","0","G","1","0","3","G","3","1","0","1","1","0","3","G","3","1","1","1"}, 
			{"2","0","2","3","1","0","1","1","1","1","0","1","1","0","1","1","1","1","1","1"}, 
			{"1","0","1","0","0","0","0","0","0","0","0","1","1","0","1","0","0","0","0","1"}, 
			{"1","0","1","0","1","1","1","1","1","1","1","1","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","1","1","1","1","1","1","1","0","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","2","0","0","0","0","0","2","0","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","3","G","3","1","1","0","0","0","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","2","2","2","1","1","0","1","1","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","0","0","0","0","0","0","1","1","1","0","0","0","1","1","0","1"}, 
			{"1","0","1","1","2","1","1","1","2","2","1","1","1","1","1","1","1","1","0","1"}, 
			{"1","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","1"}, 
			{"1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"}
		};
		string[,] faceMapLeft = new string[,]
		{
			{"1","2","2","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"}, 
			{"1","2","0","0","0","0","1","0","0","0","0","0","0","0","0","1","1","1","1","1"}, 
			{"1","2","2","2","2","0","1","0","2","2","2","2","2","2","0","1","1","1","1","1"}, 
			{"1","0","0","2","2","0","1","0","0","0","0","0","0","2","0","2","2","2","2","3"}, 
			{"1","0","0","2","2","0","1","0","2","2","0","0","0","2","0","2","0","0","0","G"}, 
			{"1","0","0","0","0","0","1","0","1","2","2","2","2","2","0","2","0","2","2","3"}, 
			{"1","1","1","1","1","0","1","0","1","1","0","0","0","0","0","0","0","1","1","1"}, 
			{"2","2","2","3","1","0","1","0","1","1","0","1","1","0","0","0","0","1","1","1"}, 
			{"2","0","0","G","1","0","3","G","3","1","0","1","1","0","3","G","3","1","1","1"}, 
			{"2","0","2","3","1","0","1","1","1","1","0","1","1","0","1","1","1","1","1","1"}, 
			{"1","0","1","0","0","0","0","0","0","0","0","1","1","0","1","0","0","0","0","1"}, 
			{"1","0","1","0","1","1","1","1","1","1","1","1","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","1","1","1","1","1","1","1","0","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","2","0","0","0","0","0","2","0","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","3","G","3","1","1","0","0","0","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","2","2","2","1","1","0","1","1","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","0","0","0","0","0","0","1","1","1","0","0","0","1","1","0","1"}, 
			{"1","0","1","1","2","1","1","1","2","2","1","1","1","1","1","1","1","1","0","1"}, 
			{"1","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","1"}, 
			{"1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"}
		};
		string[,] faceMapRight = new string[,]
		{
			{"1","2","2","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"}, 
			{"1","2","0","0","0","0","1","0","0","0","0","0","0","0","0","1","1","1","1","1"}, 
			{"1","2","2","2","2","0","1","0","2","2","2","2","2","2","0","1","1","1","1","1"}, 
			{"1","0","0","2","2","0","1","0","0","0","0","0","0","2","0","2","2","2","2","3"}, 
			{"1","0","0","2","2","0","1","0","2","2","0","0","0","2","0","2","0","0","0","G"}, 
			{"1","0","0","0","0","0","1","0","1","2","2","2","2","2","0","2","0","2","2","3"}, 
			{"1","1","1","1","1","0","1","0","1","1","0","0","0","0","0","0","0","1","1","1"}, 
			{"2","2","2","3","1","0","1","0","1","1","0","1","1","0","0","0","0","1","1","1"}, 
			{"2","0","0","G","1","0","3","G","3","1","0","1","1","0","3","G","3","1","1","1"}, 
			{"2","0","2","3","1","0","1","1","1","1","0","1","1","0","1","1","1","1","1","1"}, 
			{"1","0","1","0","0","0","0","0","0","0","0","1","1","0","1","0","0","0","0","1"}, 
			{"1","0","1","0","1","1","1","1","1","1","1","1","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","1","1","1","1","1","1","1","0","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","2","0","0","0","0","0","2","0","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","3","G","3","1","1","0","0","0","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","2","2","2","1","1","0","1","1","1","0","1","0","1","1","0","1"}, 
			{"1","0","1","0","0","0","0","0","0","0","1","1","1","0","0","0","1","1","0","1"}, 
			{"1","0","1","1","2","1","1","1","2","2","1","1","1","1","1","1","1","1","0","1"}, 
			{"1","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","1"}, 
			{"1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"}
		};
		string[,] faceMapFront = new string[,]
		{
			{"2","1","1","1","2","1","1","1","1","1","2","2","2","1","2","2","1","2","1","2"}, 
			{"2","1","1","1","1","2","1","2","1","1","2","3","2","1","1","1","1","1","2","2"}, 
			{"1","1","1","2","1","1","1","0","0","0","0","G","2","1","2","1","2","1","2","1"}, 
			{"1","1","2","1","1","2","1","0","1","1","2","3","2","1","1","1","1","1","2","1"}, 
			{"2","1","2","1","1","1","2","0","1","1","2","2","2","1","2","2","1","1","2","1"}, 
			{"1","1","1","2","1","0","0","0","0","0","0","0","0","0","0","0","0","0","1","1"}, 
			{"1","1","2","1","1","0","3","2","3","0","1","2","1","1","1","1","1","0","1","1"}, 
			{"1","1","2","1","1","0","1","1","1","0","1","1","1","1","1","1","2","0","1","1"}, 
			{"1","1","1","1","1","0","3","2","3","0","1","2","1","1","1","1","1","0","2","1"}, 
			{"1","1","2","1","1","0","0","0","0","0","1","1","1","1","2","1","2","0","2","1"}, 
			{"1","2","2","2","1","0","1","2","1","1","1","1","1","1","2","1","1","0","2","2"}, 
			{"1","2","3","2","1","0","2","1","2","1","2","1","1","1","1","1","2","0","2","1"}, 
			{"1","2","G","0","0","0","1","2","1","1","0","0","0","0","0","0","0","0","1","1"}, 
			{"1","2","3","2","1","2","1","1","1","1","0","0","8","0","0","1","2","1","1","1"}, 
			{"1","2","2","2","2","1","2","1","2","1","0","0","0","0","0","1","1","1","1","1"}, 
			{"2","1","1","0","0","0","0","0","0","0","0","1","1","1","1","1","2","2","2","1"}, 
			{"1","1","1","0","2","1","1","1","1","1","1","1","1","1","1","1","2","3","2","2"}, 
			{"1","2","2","0","0","0","0","0","0","0","0","0","0","0","0","0","0","G","2","1"}, 
			{"1","1","2","2","1","1","1","1","1","1","1","1","1","1","1","1","2","3","2","1"}, 
			{"1","2","1","2","2","1","1","2","1","1","1","1","1","1","2","1","2","2","2","1"}
		};
		string[,] faceMapBack = new string[,]
		{
			{"2","2","2","2","1","2","1","1","1","1","1","1","1","1","1","2","2","2","1","2"}, 
			{"3","G","3","1","2","2","2","2","0","0","0","0","0","0","0","0","0","0","0","2"}, 
			{"2","0","1","1","2","2","1","2","0","2","2","1","1","2","2","1","2","2","0","2"}, 
			{"2","0","0","0","2","1","1","3","G","3","1","1","0","0","0","1","2","2","0","2"}, 
			{"1","2","3","0","3","1","2","2","2","1","1","2","0","3","0","0","0","0","0","1"}, 
			{"1","2","1","0","0","0","0","0","0","0","0","0","0","2","0","2","1","2","0","2"}, 
			{"1","1","2","1","1","0","2","1","1","2","1","2","2","3","0","1","1","1","0","2"}, 
			{"1","0","0","0","0","0","0","0","0","0","0","0","0","0","0","2","2","1","0","2"}, 
			{"1","0","2","1","1","1","1","2","2","2","1","2","2","2","1","2","1","1","0","2"}, 
			{"2","0","2","0","0","0","0","2","1","2","1","1","0","0","0","0","2","1","0","2"}, 
			{"1","0","2","0","2","1","0","2","2","1","2","1","0","2","3","0","2","2","0","1"}, 
			{"1","0","2","0","1","1","0","2","0","0","0","0","0","2","2","0","2","1","0","2"}, 
			{"2","0","0","0","2","2","0","1","0","3","2","2","2","2","3","0","1","2","0","1"}, 
			{"2","2","1","1","2","2","0","1","0","2","2","2","0","0","0","0","1","1","0","1"}, 
			{"2","1","1","2","1","2","0","1","0","0","0","2","0","2","1","1","2","3","0","2"}, 
			{"2","2","0","0","0","0","0","1","1","1","0","1","0","2","0","0","0","0","0","2"}, 
			{"1","2","0","2","1","1","0","2","1","2","0","2","0","1","0","0","0","3","2","2"}, 
			{"2","2","0","1","1","1","0","0","0","0","0","1","0","1","2","3","0","1","2","2"}, 
			{"2","3","G","3","1","2","2","1","1","2","2","2","0","0","0","0","0","2","2","2"}, 
			{"1","2","1","2","1","1","2","1","2","1","2","2","2","2","1","1","2","1","1","2"}
		};
		
		List<string[,]> faceMaps = new List<string[,]>();
		faceMaps.Add(faceMapTop);//Top
		faceMaps.Add(faceMapBottom);//Bottom
		faceMaps.Add(faceMapLeft);//Left
		faceMaps.Add(faceMapRight);//Right
		faceMaps.Add(faceMapFront);//Front
		faceMaps.Add(faceMapBack);//Back

		Material grassMaterial = Resources.Load<Material>("Materials/Environment/Forest/forest-floor");

		// Build inner cube from a list of face maps
		WorldCube.BuildInnerCubeFromFaceList(faceMaps, 4.0f, grassMaterial);
	}

	void Update()
	{

	}
}

