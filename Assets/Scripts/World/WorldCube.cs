using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldCube : MonoBehaviour
{
	#region Fields and Properties

	private const float TORQUE_MOD = 5.0f;
	private const float CUBE_PLANE_THICKNESS = 0.01f;

	public GameObject OuterCube {get; set;}
	public List<GameObject> InnerCubes {get; set;}

	private bool mouseIsDown;
	private float previousPointerX;
	private float previousPointerY;
	private float angleLerpTime;
	private bool rotatingHorizontal;
	private bool rotatingVertical;

	#endregion

	#region Lifecycle

	void Awake()
	{
		InnerCubes = new List<GameObject>();
		previousPointerX = 0;
		previousPointerY = 0;
		angleLerpTime = 0;
		mouseIsDown = false;
		rotatingHorizontal = false;
		rotatingVertical = false;
	}

	void Start()
	{
		BoxCollider boxCollider = gameObject.AddComponent<BoxCollider>();
		boxCollider.isTrigger = false;

		gameObject.isStatic = true;
	}

	void Update()
	{
		if (GameManager.SharedInstance.Camera3D.IsAttachedToPlayer == false)
		{
			HandleUserInput();
		}
	}

	#endregion

	#region Cube Construction
	
	private Matrix4x4 GetTransformForFaceIndex(int faceIndex)
	{
		switch(faceIndex)
		{
		case 0: // Top
			return Matrix4x4.identity;
		case 1: // Bottom
			return Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(new Vector3(180, 180, 0)), new Vector3(1,1,1));
		case 2: // Left
			return Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(new Vector3(270, 0, 90)), new Vector3(1,1,1));
		case 3: // Right
			return Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(new Vector3(270, 0, 270)), new Vector3(1,1,1));
		case 4: // Front
			return Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(new Vector3(270, 0, 0)), new Vector3(1,1,1));
		case 5: // Back
			return Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(new Vector3(270, 180, 0)), new Vector3(1,1,1));
		default:
			return Matrix4x4.identity;
		}
	}

	private Vector3 GetLocalRotationForFaceIndex(int faceIndex)
	{
		switch(faceIndex)
		{
		case 0: // Top
			return Vector3.zero;
		case 1: // Bottom
			return new Vector3(180, 0, 0);
		case 2: // Left
			return new Vector3(0, 0, 90);
		case 3: // Right
			return new Vector3(0, 0, 270);
		case 4: // Front
			return new Vector3(270, 0, 0);
		case 5: // Back
			return new Vector3(90, 0, 0);
		default:
			return Vector3.zero;
		}
	}

	public void BuildOuterCube(float size, Material material)
	{
		OuterCube = CreateDoubleSidedCube(size, size, size, Vector3.zero, material);
		OuterCube.AddComponent<MeshCollider>();
	}

	public void BuildInnerCubeFromFaceList(List<string[,]> faceMaps, float parentCubeSize, Material material)
	{
		GameObject parentCube = CreateDoubleSidedCube(parentCubeSize, parentCubeSize, parentCubeSize, Vector3.zero, material);
		InnerCubes.Add(parentCube);

		for (int i = 0; i < faceMaps.Count; i++)
		{
			Matrix4x4 faceTransform = GetTransformForFaceIndex(i);

			string[,] faceMap = faceMaps[i];

			float cubeSize = (parentCubeSize + (CUBE_PLANE_THICKNESS*2)) / faceMap.GetLength(0);
			float verticalSize = 0.25f;

			float xOffset = (cubeSize * ((float)faceMap.GetLength(1)/2.0f)) - (cubeSize/2);
			float yOffset = (cubeSize * ((float)faceMap.GetLength(0)/2.0f)) - (cubeSize/2);

			for (int y = 0; y < faceMap.GetLength(1); y++)
			{
				for (int x = 0; x < faceMap.GetLength(0); x++)
				{
					string faceMapValue = faceMap[y,x];
					string faceMapMetaValue = "";
					if (faceMapValue.Length > 1)
					{
						faceMapMetaValue = faceMapValue.Substring(1);
					}

					if (faceMapValue != "0")
					{
						Vector3 position = new Vector3((cubeSize*x) - xOffset, 
						                               (cubeSize*((float)faceMap.GetLength(1)/2.0f))+(verticalSize/2)-CUBE_PLANE_THICKNESS, 
						                               (cubeSize*-y) + yOffset);
						faceTransform = Matrix4x4.TRS(new Vector3(-position.x, -position.y, -position.z), Quaternion.identity, new Vector3(1,1,1)) * faceTransform;
						position = faceTransform.MultiplyVector(position);

						CubeSurfaceInfo cubeSurfaceInfo = CubeMapConfig.CubeMapSurfaceTypes[faceMap[y,x]];
						
						if (cubeSurfaceInfo != null)
						{
							if (cubeSurfaceInfo.IsPlayerStart == true)
							{
								// Player start
								GameManager.SharedInstance.EntityManager.CreatePlayer(position, gameObject.transform);
							}
							else
							{
								GameObject childCube = null;

								// TODO: Customisation point - Add other cases for custom game objects e.g. switches, trees etc.
								if (cubeSurfaceInfo.HasPrefabFile() == false && cubeSurfaceInfo.IsBlock == true)
								{
									Material cubeMaterial = Resources.Load<Material>(cubeSurfaceInfo.MaterialFile);
									childCube = CreateCube(parentCube.transform, cubeSize, verticalSize, cubeSize, position, Vector3.zero, cubeMaterial);
								}
								else
								{
									childCube = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>(cubeSurfaceInfo.PrefabFile), 
									                                               Vector3.zero, 
									                                               Quaternion.identity);
									MeshFilter[] allMeshFilters = childCube.GetComponentsInChildren<MeshFilter>();
									verticalSize = 0;
									for (int f = 0; f < allMeshFilters.Length; f++)
									{
										Bounds meshBounds = allMeshFilters[f].mesh.bounds;

										verticalSize += meshBounds.size.y;
									}
									BoxCollider boxCollider = childCube.AddComponent<BoxCollider>();
									boxCollider.size = new Vector3(cubeSize, verticalSize,cubeSize);

								 	position = new Vector3((cubeSize*x) - xOffset, 
									                       (cubeSize*((float)faceMap.GetLength(1)/2.0f))+(verticalSize/2)-CUBE_PLANE_THICKNESS, 
							                               (cubeSize*-y) + yOffset);
									position = faceTransform.MultiplyVector(position);

									// It's a gateway!, time for some special stuff
									if (faceMapValue == "G")
									{
										boxCollider.isTrigger = true;
										Gateway gateway = childCube.AddComponent<Gateway>();
										gateway.GatewayId = faceMapMetaValue;
									}


									childCube.transform.parent = parentCube.transform;
									childCube.transform.localPosition = position;
								}
								childCube.transform.Rotate(GetLocalRotationForFaceIndex(i));
								InnerCubes.Add(childCube);
							}
						}
					}
				}
			}
		}

	}

	#endregion

	#region World Construction

	public GameObject CreateCube(Transform parentTransform, float width, float height, float depth, Vector3 position, Vector3 rotation, Material material)
	{
		GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
		cube.transform.parent = parentTransform;
		cube.transform.localScale = new Vector3(width, height, depth);
		cube.transform.localPosition = position;
		cube.transform.Rotate(rotation);

		cube.renderer.material = material;

		return cube;
	}

	public GameObject CreateCubePlane(Transform parentTransform, float width, float height, Vector3 position, Vector3 rotation, string name)
	{
		GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Cube);
		plane.name = name;
		plane.transform.parent = parentTransform;
		plane.transform.localScale = new Vector3(width+CUBE_PLANE_THICKNESS, CUBE_PLANE_THICKNESS, height+CUBE_PLANE_THICKNESS);
		plane.transform.localPosition = position;
		plane.transform.Rotate(rotation);

		return plane;
	}

	public GameObject CreateDoubleSidedCube(float width, float height, float depth, Vector3 centerPosition, Material material)
	{
		GameObject childCube = new GameObject("Child Cube");
		childCube.transform.parent = gameObject.transform;
		childCube.transform.localPosition = centerPosition;

		for (int i = 0; i < 6; i++)
		{
			GameObject plane = null;

			switch(i)
			{
			case 0: // Left
				plane = CreateCubePlane(childCube.transform, width, height, new Vector3(-(width/2), 0, 0), new Vector3(270, 90, 0), "Left"); 
				break;
			case 1: // Right
				plane = CreateCubePlane(childCube.transform, width, height, new Vector3((width/2), 0, 0), new Vector3(90, 90, 0), "Right"); 
				break;
			case 2: // Front
				plane = CreateCubePlane(childCube.transform, width, height, new Vector3(0, 0, -(depth/2)), new Vector3(270, 0, 0), "Front"); 
				break;
			case 3: // Back
				plane = CreateCubePlane(childCube.transform, width, height, new Vector3(0, 0, (depth/2)), new Vector3(90, 0, 0), "Back"); 
				break;
			case 4: // Top
				plane = CreateCubePlane(childCube.transform, width, height, new Vector3(0, (height/2), 0), new Vector3(180, 0, 0), "Top"); 
				break;
			case 5: // Bottom
				plane = CreateCubePlane(childCube.transform, width, height, new Vector3(0, -(height/2), 0), new Vector3(0, 0, 0), "Bottom"); 
				break;
			}

			plane.renderer.material = material;
		}

		return childCube;
	}

	#endregion

	#region User Input

	private void HandleUserInput()
	{
		// Player controlled rotation based on pointer diff
		if (Input.GetMouseButton(0) == true)
		{
			mouseIsDown = true;
			angleLerpTime = 0;
			float pointerX = Input.GetAxis("Mouse X");
			float pointerY = Input.GetAxis("Mouse Y");
			float diffX = previousPointerX - pointerX;
			float diffY = pointerY - previousPointerY;
		
			if (rotatingVertical == false && Mathf.Abs(diffX) > 0.2f)
			{
				rotatingHorizontal = true;
				Vector3 newRotation = new Vector3(0, diffX*TORQUE_MOD, 0);
				gameObject.transform.RotateAround(gameObject.transform.position, new Vector3(1,0,0), newRotation.x);
				gameObject.transform.RotateAround(gameObject.transform.position, new Vector3(0,1,0), newRotation.y);
				gameObject.transform.RotateAround(gameObject.transform.position, new Vector3(0,0,1), newRotation.z);
			}

			if (rotatingHorizontal == false && Mathf.Abs(diffY) > 0.2f)
			{
				rotatingVertical = true;
				Vector3 newRotation = new Vector3(diffY*TORQUE_MOD, 0, 0);
				gameObject.transform.RotateAround(gameObject.transform.position, new Vector3(1,0,0), newRotation.x);
				gameObject.transform.RotateAround(gameObject.transform.position, new Vector3(0,1,0), newRotation.y);
				gameObject.transform.RotateAround(gameObject.transform.position, new Vector3(0,0,1), newRotation.z);
			}

		}

		// Lerp to nearest flat angle after player releases
		if (Input.GetMouseButton(0) == false)
		{
			mouseIsDown = false;
			rotatingHorizontal = false;
			rotatingVertical = false;
			if (angleLerpTime < 1)
			{
				Vector3 currentRotation = gameObject.transform.rotation.eulerAngles;
				angleLerpTime += 0.025f;
				currentRotation.x = Mathf.LerpAngle(gameObject.transform.rotation.eulerAngles.x, Mathf.Round(currentRotation.x / 90) * 90, angleLerpTime);
				currentRotation.y = Mathf.LerpAngle(gameObject.transform.rotation.eulerAngles.y, Mathf.Round(currentRotation.y / 90) * 90, angleLerpTime);
				currentRotation.z = Mathf.LerpAngle(gameObject.transform.rotation.eulerAngles.z, Mathf.Round(currentRotation.z / 90) * 90, angleLerpTime);

				gameObject.transform.rotation = Quaternion.Euler(currentRotation);
			}
		}
	}

	#endregion
}

